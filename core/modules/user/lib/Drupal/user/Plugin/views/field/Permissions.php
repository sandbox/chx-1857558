<?php

/**
 * @file
 * Definition of Drupal\user\Plugin\views\field\Permissions.
 */

namespace Drupal\user\Plugin\views\field;

use Drupal\Component\Annotation\Plugin;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\field\PrerenderList;

/**
 * Field handler to provide a list of permissions.
 *
 * @ingroup views_field_handlers
 *
 * @Plugin(
 *   id = "user_permissions",
 *   module = "user"
 * )
 */
class Permissions extends PrerenderList {

  /**
   * Overrides Drupal\views\Plugin\views\field\FieldPluginBase::init().
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);

    $this->additional_fields['uid'] = array('table' => 'users', 'field' => 'uid');
  }

  public function query() {
    $this->add_additional_fields();
    $this->field_alias = $this->aliases['uid'];
  }

  function pre_render(&$values) {
    $uids = array();
    $this->items = array();

    foreach ($values as $result) {
      $uids[] = $this->get_value($result, 'uid');
    }

    if ($uids) {
      $permission_names = module_invoke_all('permission');
      $query = db_select('users_roles', 'u');
      $query->fields('u', array('uid', 'rid'));
      $query->condition('u.uid', $uids);
      $result = $query->execute();
      foreach ($result as $row) {
        foreach (config("user.permission.$row->rid")->get() as $permission) {
          $this->items[$row->uid][$permission]['permission'] = $permission_names[$permission]['title'];
        }
      }
      foreach ($uids as $uid) {
        if (isset($this->items[$uid])) {
          ksort($this->items[$uid]);
        }
      }
    }
  }

  function render_item($count, $item) {
    return $item['permission'];
  }

  /*
  function document_self_tokens(&$tokens) {
    $tokens['[' . $this->options['id'] . '-role' . ']'] = t('The name of the role.');
    $tokens['[' . $this->options['id'] . '-rid' . ']'] = t('The role ID of the role.');
  }

  function add_self_tokens(&$tokens, $item) {
    $tokens['[' . $this->options['id'] . '-role' . ']'] = $item['role'];
    $tokens['[' . $this->options['id'] . '-rid' . ']'] = $item['rid'];
  }
  */

}
