<?php

/**
 * @file
 * Contains \Drupal\user\Tests\Views\HandlerFilterPermissionTest.
 */

namespace Drupal\user\Tests\Views;

use Drupal\user\Tests\Views\UserUnitTestBase;

/**
 * Tests the permissions filter handler.
 */
class HandlerFilterPermissionTest extends UserUnitTestBase {

  /**
   * Views used by this test.
   *
   * @var array
   */
  public static $testViews = array('test_filter_permission');

  protected $columnMap;

  public static function getInfo() {
    return array(
      'name' => 'User: Permissions Filter',
      'description' => 'Tests the permission filter handler.',
      'group' => 'Views module integration',
    );
  }

  /**
   * Tests the permission filter handler.
   *
   * @todo Fix the different commented out tests by fixing the many to one
   *   handler handling with the NOT operator.
   */
  public function testFilterPermission() {
    $this->setupPermissionTestData();

    $column_map = array('uid' => 'uid');
    $view = views_get_view('test_filter_permission');

    // Filter by a non existing permission.
    $view->initHandlers();
    $view->filter['permission']->value = array('non_existent_permission');
    $this->executeView($view);
    $this->assertEqual(count($view->result), 4, 'A non existent permission is not filtered so everything is the result.');
    $expected[] = array('uid' => 1);
    $expected[] = array('uid' => 2);
    $expected[] = array('uid' => 3);
    $expected[] = array('uid' => 4);
    $this->assertIdenticalResultset($view, $expected, $column_map);
    $view->destroy();

    // Filter by a non existing permission with operator NOT.
    // $view->initHandlers();
    // $view->filter['permission']->value = array('Non existent permission');
    // $view->filter['permission']->operator = 'not';
    // $this->executeView($view);
    // $this->assertEqual(count($view->result), count($this->users), 'A non existent permission with the NOt operator should result in every user');
    // $expected = array();
    // $expected[] = array('uid' => 1);
    // $expected[] = array('uid' => 2);
    // $expected[] = array('uid' => 3);
    // $expected[] = array('uid' => 4);
    // $this->assertIdenticalResultset($view, $expected, $column_map);
    // $view->destroy();

    // Filter by a permission.
    $view->initHandlers();
    $view->filter['permission']->value = array('administer permissions');
    $this->executeView($view);
    $this->assertEqual(count($view->result), 2);
    $expected = array();
    $expected[] = array('uid' => 3);
    $expected[] = array('uid' => 4);
    $this->assertIdenticalResultset($view, $expected, $column_map);
    $view->destroy();

    // Filter by a permission with operator NOT.
    // $view->initHandlers();
    // $view->filter['permission']->value = array('administer permissions');
    // $view->filter['permission']->operator = 'not';
    // $this->executeView($view);
    // $this->assertEqual(count($view->result), 2);
    // $expected = array();
    // $expected[] = array('uid' => 1);
    // $expected[] = array('uid' => 2);
    // $this->assertIdenticalResultset($view, $expected, $column_map);
    // $view->destroy();

    // Filter by another permission of a role with multiple permissions.
    $view->initHandlers();
    $view->filter['permission']->value = array('administer users');
    $this->executeView($view);
    $this->assertEqual(count($view->result), 1);
    $expected = array();
    $expected[] = array('uid' => 4);
    $this->assertIdenticalResultset($view, $expected, $column_map);
    $view->destroy();

    // Filter by another permission of a role with multiple permissions and
    // operator NOT.
    // $view->initHandlers();
    // $view->filter['permission']->value = array('administer users');
    // $view->filter['permission']->operator = 'not';
    // $this->executeView($view);
    // $this->assertEqual(count($view->result), 3);
    // $expected = array();
    // $expected[] = array('uid' => 1);
    // $expected[] = array('uid' => 2);
    // $expected[] = array('uid' => 3);
    // $this->assertIdenticalResultset($view, $expected, $column_map);
    // $view->destroy();

    // @todo Test the available UI options.
  }

}
